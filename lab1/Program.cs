﻿using System;
using System.Collections.Generic;
using MySqlConnector;

namespace lab1
{
    internal class Program
    {
        #region Private Methods

        private static void Main(string[] args)
        {
            string connectionString = @"Server=localhost;Database=phone_calls;Uid=root;Pwd=romko12345;";

            string sqlExpression =
                "SELECT p.id, p.first_name, p.second_name, p.last_name, p.phone_number, " +
                "a.city, a.street, a.house_number, COUNT(DISTINCT c.destination) as DISTINCT_CITIES" +
                " FROM address as a " +
                " INNER JOIN person as p ON a.id = p.address " +
                " left outer JOIN calls as c ON c.user_id = p.id " +
                " GROUP BY p.id";


            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                connection.Open();
                MySqlCommand command = new MySqlCommand(sqlExpression, connection);
                MySqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        object firstName = reader.GetValue(1);
                        object secondName = reader.GetValue(2);
                        object lastName = reader.GetValue(3);
                        object phoneNumber = reader.GetValue(4);
                        object city = reader.GetValue(5);
                        object street = reader.GetValue(6);
                        object houseNumber = reader.GetValue(7);
                        object countCity = reader.GetValue(8);


                        Console.WriteLine(
                            $"First name: {firstName};  Second name: {secondName}; Last name {lastName}, " +
                            $"Phone number: {phoneNumber}; City: {city}; Street {street}; House number {houseNumber}; Count city: {countCity}");
                    }
                }

                reader.Close();
            

            sqlExpression =
                "SELECT p.id, p.first_name, p.second_name, p.last_name, p.phone_number " +
                " FROM person as p " ;


            List<(int id, string firstName, string lastName)> people =
                new List<(int id, string firstName, string lastName)>();

      
                MySqlCommand command2 = new MySqlCommand(sqlExpression, connection);
                MySqlDataReader reader2 = command2.ExecuteReader();

                if (reader2.HasRows)
                {
                    while (reader2.Read())
                    {
                        object id = reader2.GetValue(0);
                        object firstName = reader2.GetValue(1);
                        object secondName = reader2.GetValue(2);
                        object lastName = reader2.GetValue(3);
                        people.Add((Convert.ToInt32(id), firstName as string, lastName as string));
                    }
                }

                reader2.Close();

                foreach (var item in people)
                {
                    string getById =
                        " SELECT c.code, c.city " +
                        " FROM calls " +
                        " INNER JOIN person as p ON calls.user_id = p.id " +
                        " INNER JOIN cities as c ON c.id = calls.destination " +
                        $"Where p.id = {item.id}";

                    MySqlCommand commandGetById = new MySqlCommand(getById, connection);
                    MySqlDataReader readerGetById = commandGetById.ExecuteReader();
                    Console.WriteLine();
                    Console.WriteLine($"First name: {item.firstName};  Second name: {item.lastName}");
                    
                    {
                        while (readerGetById.Read())
                        {
                            object cod = readerGetById.GetValue(0);
                            object city = readerGetById.GetValue(1);

                            Console.WriteLine(
                                $"Cod: {cod}; City: {city}, ");
                        }
                    }

                    readerGetById.Close();
                }

                Console.ReadKey();
            }

            #endregion Private Methods
        }
    }
}
