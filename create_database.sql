USE `phone_calls`;
CREATE TABLE `calls` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `user_id` int,
  `date_of_calls` datetime,
  `duration` float,
  `destination` int
);

CREATE TABLE `person` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `first_name` varchar(255),
  `second_name` varchar(255),
  `last_name` varchar(255),
  `phone_number` varchar(255),
  `address` int
);

CREATE TABLE `address` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `city` varchar(255),
  `street` varchar(255),
  `house_number` varchar(255)
);

CREATE TABLE `cities` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `price` float,
  `city` varchar(255),
  `code` int
);

ALTER TABLE `calls` ADD FOREIGN KEY (`user_id`) REFERENCES `person` (`id`);

ALTER TABLE `calls` ADD FOREIGN KEY (`destination`) REFERENCES `cities` (`id`);

ALTER TABLE `person` ADD FOREIGN KEY (`address`) REFERENCES `address` (`id`);
